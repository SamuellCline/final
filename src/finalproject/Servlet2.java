package finalproject;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "Servlet2", urlPatterns={"/Servlet2"})
public class Servlet2 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        writer.println("<html><head><style>\n" +
                "    .button1 {\n" +
                "        -webkit-appearance: button;\n" +
                "        -moz-appearance: button;\n" +
                "        appearance: button;\n" +
                "        text-decoration: none;\n" +
                "        color: black;\n" +
                "        margin: 10px;\n" +
                "        padding: 5px;\n" +
                "        border-radius: 3px;\n" +
                "        background-color:darkgray;\n" +
                "        min-width: 300px;\n" +
                "        text-align: center;\n" +
                "    }\n" +
                "</style></head><body>");
//

        String x = request.getParameter("search1");
        String y = request.getParameter("search2");

        QueriesStuff q = QueriesStuff.getInstance();


        List<Motorcycle> list = new ArrayList();
        list = q.getMotorcycle(x,y);
        for (Motorcycle element :list) {
            writer.println(element + "<br>");
        }

        String a = request.getContextPath();
        writer.println("<a class=\"button1\" href=\"" + a + "/index.jsp\">Main Menu</a>");
        writer.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("This resource is not available");
    }
}
