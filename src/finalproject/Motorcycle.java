package finalproject;
import javax.persistence.*;
import javax.validation.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Entity
@Table(name = "Motorcycles")
public class Motorcycle {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "Id")
        private int Id;
    @Positive
    @Min(value = 1885) //Year first Motorcycle was built
    @Column(name = "Year")
        private int Year;
    @NotBlank
    @Column(name = "Make")
        private String Make;
    @NotBlank
    @Column(name = "Model")
        private String Model;
    @Positive
    @Column(name = "Size")
        private int Size;






    public int getId() { return Id; }
    public void setId(int Id){ this.Id = Id; }
    public int getYear(){
            return Year;
    }
    public void setYear(int Year){
            this.Year = Year;
    }
    public String getMake(){
        return Make;
    }
    public void setMake(String Make){
        this.Make = Make;
    }
    public String getModel(){
        return Model;
    }
    public void setModel(String Model){
        this.Model = Model;
    }
    public int getSize(){
        return Size;
    }
    public void setSize(int Size){
        this.Size = Size;
    }



    public String toString() {
        return "Year: " + Year + System.getProperty("line.separator") + "Make: " + Make + System.getProperty("line.separator") + "Model: " + Model + System.getProperty("line.separator") + "Engine Displacement: " + Size;
    }


}
