package finalproject;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        writer.println("<html><head><style>\n" +
                "    .button1 {\n" +
                "        -webkit-appearance: button;\n" +
                "        -moz-appearance: button;\n" +
                "        appearance: button;\n" +
                "        text-decoration: none;\n" +
                "        color: black;\n" +
                "        margin: 10px;\n" +
                "        padding: 5px;\n" +
                "        border-radius: 3px;\n" +
                "        background-color:darkgray;\n" +
                "        min-width: 300px;\n" +
                "        text-align: center;\n" +
                "    }\n" +
                "</style></head><body>");

            writer.println();
        int year = Integer.parseInt(request.getParameter("Year1"));
        String make = request.getParameter("Make1");
        String model = request.getParameter("Model1");
        int displacement = Integer.parseInt(request.getParameter("Displacement1"));
        QueriesStuff q = QueriesStuff.getInstance();
        try {
            q.addMotorcycle(year, make, model, displacement);
            writer.println("<h1>What you entered:</h1>" +
                    "Year: " + year + ", Make: " + make + ", Model: " + model + ", Displacement:" + displacement);
        }
        catch (Exception e){
            writer.println("<h1>" + e + "</h1>");
        }
        String a = request.getContextPath();
        writer.println("<a class=\"button1\" href=\"" + a + "/index.jsp\">Main Menu</a>");
        writer.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("This resource is not available");
    }
}
