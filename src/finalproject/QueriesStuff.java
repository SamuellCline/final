package finalproject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;


import javax.validation.*;

import java.util.*;


public class QueriesStuff {

    SessionFactory factory = null;
    Session session = null;

    private static QueriesStuff single_instance = null;

    private QueriesStuff() {
        factory = HibernateUtilities.getSessionFactory();
    }


    public static QueriesStuff getInstance() {
        if (single_instance == null) {
            single_instance = new QueriesStuff();
        }

        return single_instance;
    }

    //lists all motorcycles
    public List<Motorcycle> getMotorcycles() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from finalproject.Motorcycle";
            List<Motorcycle> lms = (List<Motorcycle>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return lms;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    //Gets single motorcycle by Year
    public List<Motorcycle> getMotorcycle(String X, String Y) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from finalproject.Motorcycle where " + X + "=" + "'" + Y + "'" ;
            List<Motorcycle> m = (List<Motorcycle>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return m;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Motorcycle addMotorcycle( int Y, String M, String Mo, int S) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();

            Motorcycle mc = new Motorcycle();
            mc.setYear(Y);
            mc.setMake(M);
            mc.setModel(Mo);
            mc.setSize(S);

            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();
            Set<ConstraintViolation<Motorcycle>> violations = validator.validate(mc);
            for (ConstraintViolation<Motorcycle> violation : violations) {


                throw new Exception(violation.getMessage());
            }


                    session.save(mc);

            session.getTransaction().commit();
            return mc;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        }
        finally
        {
            session.close();
        }

    }
}
