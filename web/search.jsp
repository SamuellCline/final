<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .button1 {
            -webkit-appearance: button;
            -moz-appearance: button;
            appearance: button;
            text-decoration: none;
            color: black;
            margin: 10px;
            padding: 5px;
            border-radius: 3px;
            background-color:darkgray;
            min-width: 300px;
            text-align: center;
        }
    </style>
</head>
<body>
<form action="Servlet2" method="post">
    <label for="search">Choose a search type:</label>
    <select id="search" name="search1">
        <option value="Year">Year</option>
        <option value="Make">Make</option>
        <option value="Model">Model</option>
    </select>

    <p>Value:<input name="search2" type="text" /></p>


    <input class="button1" type="submit" value="Submit">
</form>
<a class="button1" href="${pageContext.request.contextPath}/index.jsp">Main Menu</a>
</body>
</html>