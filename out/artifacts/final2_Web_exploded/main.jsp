<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .button1 {
            -webkit-appearance: button;
            -moz-appearance: button;
            appearance: button;
            text-decoration: none;
            color: black;
            margin: 10px;
            padding: 5px;
            border-radius: 3px;
            background-color:darkgray;
            min-width: 300px;
            text-align: center;
        }
    </style>
</head>
<body>
<form action="Servlet" method="post">
<!--<p>VIN:<input name="Vin1" type="number" /></p>-->
<p>Year:<input name="Year1" type="number" /></p>
<p>Make:<input name="Make1" type="text" /></p>
<p>Model:<input name="Model1" type="text" /></p>
<p>Displacement:<input name="Displacement1" type="number" /></p>
<input class="button1" type="submit" value="Submit">
</form>

<a class="button1" href="${pageContext.request.contextPath}/index.jsp">Main Menu</a>

</body>
</html>